
//Header toggle
let MenuBtn=document.getElementById('MenuBtn');
let body=document.querySelector('body');
MenuBtn.addEventListener('click',function(e){
    body.classList.toggle('mobile-nav-active');
    this.classList.toggle('fa-xmark');
})
document.onclick=function(e){
    if(e.target.id !== 'MenuBtn' && e.target.id!=='body'){
        body.classList.remove('mobile-nav-active')
        MenuBtn.classList.remove('fa-xmark');
    }
}
//Typing Effect
let typed=new Typed('.auto-input',{
    strings:['Student!','Coder!','Singer!','Tiktoker!'],
    typeSpeed: 100,
    backSpeed: 100,
    backDelay: 1000,
    loop: true,});
//Active Link State On Scroll

//Get All Links
let navLinks=document.querySelectorAll('nav ul li a');
//Get all Sections
let Sections=document.querySelectorAll('section');
window.addEventListener('scroll',function(){
    const scrollPos=window.scrollY +20
    Sections.forEach(section=>{
        if(scrollPos>section.offsetTop && scrollPos<(section.offsetTop + section.offsetHeight)){
            navLinks.forEach(link=>{
                link.classList.remove('active');
                if(section.getAttribute('id')===link.getAttribute('href').substring(1)){
                    link.classList.add('active');
                }
            });
        }
    });
});
document.addEventListener('DOMContentLoaded', function() {
    const messageForm = document.getElementById('messageForm');
    const subjectInput = document.getElementById('subjectInput');
    const descriptionInput = document.getElementById('descriptionInput');
    const messageStatus = document.getElementById('messageStatus');
    const yourname=document.getElementById('yourname');
    const youremail=document.getElementById('youremail');

    messageForm.addEventListener('submit', function(event) {
        event.preventDefault();

        const subject = subjectInput.value.trim();
        const description = descriptionInput.value.trim();
        const name=yourname.value.trim();
        const email=youremail.value.trim();

        // Clear the input fields
        if(name && email && subject && description){
            subjectInput.value = '';
            descriptionInput.value = '';
            yourname.value='';
            youremail.value='';

            // Display the message status
             messageStatus.textContent = 'Message sent successfully you will get my replay soon!';
        }else{
            messageStatus.textContent="please fill in all fields!"
        }
        
    });
});
